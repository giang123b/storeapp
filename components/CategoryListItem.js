import React from "react";
import { Image, Text, View, StyleSheet } from "react-native";
import skiImage from "../assets/ski.png";

export default function CategoryListItem(props) {
  return (
    <View style={styles.container}>
      <Text style={styles.tittle}>Category List Item</Text>
      <Image style={styles.categoryImage} source={skiImage}></Image>
    </View>
  );
}

const styles = StyleSheet.create({
  categoryImage: {
    width: 64,
    height: 64,
    alignItems: "center",
  },

  container: {
    alignItems: "center",
    padding: 16,
    borderRadius: 4,
    backgroundColor: "#FFF",
    shadowColor: "#000",
    shadowOpacity: 0.3,
    shadowRadius: 10,
    shadowOffset: { width: 0, height: 0 },
    marginBottom: 16,
  },

  tittle: {
    textTransform: "uppercase",
    marginBottom: 8,
  },
});
